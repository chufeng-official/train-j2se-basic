package com.bosssoft.hr.train.j2se.example;

import com.bosssoft.hr.train.j2se.example.util.UtilDemo;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;

/**
 * 工具测试类
 * 测试UtilDemo中的8个方法
 */
public class UtilTest {
    public static void test() throws InvocationTargetException, IllegalAccessException, UnsupportedEncodingException {
        System.out.println("方法一执行：");
        UtilDemo.method1();
        System.out.println("方法二执行：");
        UtilDemo.method2();
        System.out.println("方法三执行：");
        UtilDemo.method3();
        System.out.println("方法四执行：");
        UtilDemo.method4();
        System.out.println("方法五执行：");
        UtilDemo.method5();
        System.out.println("方法六执行：");
        UtilDemo.method6();
        System.out.println("方法七执行：");
        UtilDemo.method7();
        System.out.println("方法八执行：");
        UtilDemo.method8();
    }
}
