package com.bosssoft.hr.train.j2se.example;

import com.bosssoft.hr.train.j2se.example.collection.*;

/**
 * 集合测试类
 * 用于测试哈希表、哈希集和树集
 */
public class CollectionTest {
    public static void test() throws InterruptedException {
        // 哈希表测试
        HashMapDemo hashMapDemo = new HashMapDemo(10);
        System.out.println("哈希表迭代输出：");
        hashMapDemo.print();
        // 哈希集测试
        HashSetDemo hashSetDemo = new HashSetDemo(10);
        System.out.println("哈希集迭代输出：");
        hashSetDemo.print();
        // 树集测试
        TreeSetDemo treeSetDemo = new TreeSetDemo(10);
        System.out.println("树集迭代输出：");
        treeSetDemo.print();
        // 多线程测试
        ThreadArrayListDemo threadArrayListDemo = new ThreadArrayListDemo();
        System.out.println("多线程输出：");
        threadArrayListDemo.startTesting();
    }
}
