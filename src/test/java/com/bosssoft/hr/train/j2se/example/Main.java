package com.bosssoft.hr.train.j2se.example;

import com.bosssoft.hr.train.j2se.example.exception.ExceptionDemo;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main(String[] args) throws InterruptedException, UnsupportedEncodingException, InvocationTargetException, IllegalAccessException {
        CollectionTest.test();
        UtilTest.test();
        OssTest.test();
        ExceptionDemo.test();
    }
}
