/**
 * Copyright (C), -2025, www.bosssof.com.cn
 *
 * @FileName HashSetDemo.java
 * @Author Administrator
 * @Date 2022-10-21  16:23
 * @Description对应结合框架作业 第2题
 * History:
 * <author> Administrator
 * <time> 2022-10-21  16:23
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.bosssoft.hr.train.j2se.example.collection;

import com.bosssoft.hr.train.j2se.example.pojo.User;

import java.util.HashSet;

/**
 * 哈希集作业
 */
public class HashSetDemo {
    HashSet<User> userHashSet = new HashSet<>();

    /**
     * 创建由User对象组成的哈希集
     *
     * @param number User对象的数量
     */
    public HashSetDemo(int number) {
        // 使用HashSet存储10个User
        for (int index = 0; index < number; index++) {
            User user = new User((long) index, Integer.toString(index), Integer.toString(index));
            userHashSet.add(user);
        }
    }

    /**
     * 在命令行输出哈希集中的所有User实例的名字
     */
    public void print() {
        // 迭代输出
        for (User user : userHashSet) {
            System.out.println(user.getName());
        }
    }
}
