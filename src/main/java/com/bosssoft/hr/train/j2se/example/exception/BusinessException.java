/**
 * Copyright (C), -2025, www.bosssof.com.cn
 *
 * @FileName BusinessException.java
 * @Author Administrator
 * @Date 2022-10-24  17:24
 * @Description 按照java基础作业 (三)	异常处理类作业 定义异常业务类
 * History:
 * <author> Administrator
 * <time> 2022-10-24  17:24
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.bosssoft.hr.train.j2se.example.exception;

/**
 * 业务异常
 */
public class BusinessException extends RuntimeException {
    /**
     * 异常码
     */
    public final int code;

    /**
     * @param code    异常码
     * @param message 异常消息
     * @param cause   异常对象
     */
    public BusinessException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }
}
