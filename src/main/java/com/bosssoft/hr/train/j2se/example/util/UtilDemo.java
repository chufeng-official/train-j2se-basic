/**
 * Copyright (C), 2001-2022, www.bosssof.com.cn
 * FileName: UtilDemo
 * Author: Administrator
 * Date: 2022-10-25 09:47:02
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2022-10-25 09:47:02
 * <version> 1.0.0
 * <desc> 该文件针对(五)	常用工具类作业进行了demo开发
 */
package com.bosssoft.hr.train.j2se.example.util;

import com.bosssoft.hr.train.j2se.example.pojo.User;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class UtilDemo {
    /**
     * Collections.sort() 链表原地排序
     */
    public static void method1() {
        ArrayList<Integer> integerList = new ArrayList<>();
        Random random = new Random(13);
        for (int index = 0; index < 10; index++) {
            integerList.add(random.nextInt(13));
        }
        System.out.print("产生的列表为：");
        System.out.println(integerList);
        Collections.sort(integerList);
        System.out.print("排序后：");
        System.out.println(integerList);
    }

    /**
     * CollectionUtils.isEmpty() 集合判空
     * CollectionUtils.exists() 判断迭代器中是否存在指定元素
     * CollectionUtils.addAll() 将一个数组中的所有元素添加到一个集合中
     */
    public static void method2() {
        ArrayList<Integer> integerList = new ArrayList<>();
        Random random = new Random(13);
        for (int index = 0; index < 10; index++) {
            integerList.add(random.nextInt(13));
        }
        System.out.print("产生的列表为：");
        System.out.println(integerList);
        // 判断集合是否为空
        if (CollectionUtils.isEmpty(integerList)) {
            System.out.println("列表为空");
        } else {
            System.out.println("列表不为空");
        }
        // 判断迭代器中是否存在指定元素
        String[] strings1 = {"a", "b", "c"};
        if (CollectionUtils.exists(Arrays.asList(strings1), "a"::equals)) {
            System.out.println("string1中存在a");
        }
        // 把strings2的所有元素添加到stringList里
        String[] strings2 = {"d", "e", "f"};
        List<String> stringList = new ArrayList<>();
        CollectionUtils.addAll(stringList, strings2);
        System.out.print("把strings2的所有元素添加到stringList里后的stringList：");
        System.out.println(stringList);
    }

    /**
     * Arrays.asList() 将数组转化为列表
     * Arrays.sort() 将数组原地排序
     * Arrays.copyOf() 复制一个数组并设置新数组的长度，截断或用零填充以获得指定的长度
     * Arrays.binarySearch() 对数组中的元素进行二分查找
     */
    public static void method3() {
        Integer[] integers = new Integer[5];
        Random random = new Random(13);
        for (int index = 0; index < 5; index++) {
            integers[index] = random.nextInt(5);
        }
        System.out.println("integers = " + Arrays.toString(integers));
        // 将数组转化为列表
        List<Integer> scoreList = Arrays.asList(integers);
        System.out.println("scoreList = " + scoreList);
        // 对数组进行排序
        Arrays.sort(integers);
        System.out.println("排序后的integers = " + Arrays.toString(integers));
        // 对数组中的元素进行二分查找
        System.out.println(Arrays.binarySearch(integers, 3));
        // 复制一个数组并设置新数组的长度为10
        Integer[] copyArray = Arrays.copyOf(integers, 10);
        System.out.println("copyArray = " + Arrays.toString(copyArray));
    }

    /**
     * FileUtils.listFiles()
     */
    public static void method4() {
        FileUtils.listFiles(
                new File("./"),
                new String[]{"*.java"},
                true
        );
    }

    /**
     * StringUtils.isEmpty() 判断字符串是否为空或者null
     */
    public static void method5() {
        System.out.println(StringUtils.isEmpty(""));
        System.out.println(StringUtils.isEmpty(null));
    }

    /**
     * BeanUtils.CopyProperties() 复制一个对象的属性到另一个对象上
     */
    public static void method6() throws InvocationTargetException, IllegalAccessException {
        User user = new User();
        User userCopy = new User();
        user.setCode("3033");
        user.setName("刘佳琪");
        user.setId(0L);
        System.out.println("user = " + user);
        BeanUtils.copyProperties(user, userCopy);
        System.out.println("userCopy = " + userCopy);
    }

    /**
     * DigestUtils.md5() 加密
     */
    public static void method7() {
        String password = "helloWorld";
        byte[] passwordBytes = password.getBytes(StandardCharsets.UTF_8);
        System.out.println(Arrays.toString(DigestUtils.md5(passwordBytes)));
    }

    /**
     * DigestUtils.sha256() 加密
     */
    public static void method8() {
        String password = "helloWorld";
        byte[] passwordBytes = password.getBytes(StandardCharsets.UTF_8);
        System.out.println(Arrays.toString(DigestUtils.sha3_256(passwordBytes)));
    }
}
