/**
 * Copyright (C), 2001-2022, www.bosssoft.com.cn
 * FileName: ExceptionDemo
 * Author: Administrator
 * Date: 2022-10-24 17:05:10
 * Description:
 * <p>  java基础作业异常处理的实现在本文件中
 * History:
 * <author> Administrator
 * <time> 2022-10-24 17:05:10
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.bosssoft.hr.train.j2se.example.exception;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * 在当前包下建立ExceptionDemo.java其中包含两个方法：
 * method1()内部模拟try catch系统异常然后转化抛出BusinessException。
 * method2()内部抛出throw new Exception()
 * main()调用method1和method2。
 **/
public class ExceptionDemo {
    /**
     * 按作业要求测试异常
     */
    public static void test() {
        ExceptionDemo exceptionDemo = new ExceptionDemo();
        exceptionDemo.method1();
        try {
            exceptionDemo.method2();
        } catch (Exception exception) {
            System.out.print("文件不存在：");
            exception.printStackTrace();
        }
    }

    /**
     * 内部模拟try catch系统异常，然后转化抛出BusinessException
     */
    public void method1() throws BusinessException {
        try {
            // 空指针异常
            throw new NullPointerException();
        } catch (RuntimeException runtimeException) {
            throw new BusinessException(0, "空指针异常", runtimeException);
        }
    }

    /**
     * 内部抛出 throw new Exception()
     */
    public void method2() throws FileNotFoundException {
        // 文件不存在异常
        FileInputStream file = new FileInputStream("C:\\File\\Not\\Found\\Exception");
        System.out.println(file);
    }
}
