/**
 * @Class User
 * @Description 该文件定义User 类 描述RBAC模型中的用户
 * @Author Administrator
 * @Date 2022-10-21  15:32
 * @version 1.0.0
 */
package com.bosssoft.hr.train.j2se.example.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Set;

/**
 * 用户实体类
 */
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class User extends BaseEntity {
    /**
     * 用户的角色集合
     */
    private Set<Role> roles;
    /**
     * 工号
     */
    private String code;
    /**
     * 姓名
     */
    private String name;
    /**
     * 密码
     */
    private String password;
    /**
     * 头像
     */
    private String profilePicture;
    /**
     * 生日
     */
    private Date birthday;
    /**
     * 电话号码
     */
    private String tel;
    /**
     * 电子邮箱
     */
    private String email;
    /**
     * 其他
     */
    private String other;
    /**
     * 备注
     */
    private String remark;
    /**
     * 部门ID
     */
    private Long departmentId;
    /**
     * 职位ID
     */
    private Long positionId;

    /**
     * 构造方法
     *
     * @param id   用户ID
     * @param code 用户工号
     * @param name 用户名
     */
    public User(Long id, String code, String name) {
        this.id = id;
        this.code = code;
        this.name = name;
    }

    /**
     * 重写toString方法
     *
     * @return 用户实例的字符串序列化表示
     */
    @Override
    public String toString() {
        return "User {\n" +
                "    code: '" + code + "'\n" +
                "    name: '" + name + "'\n" +
                "    id: '" + id + "'\n" +
                '}';
    }
}
