/**
 * Copyright (C), -2025, www.bosssof.com.cn
 *
 * @FileName Role.java
 * @Author Administrator
 * @Date 2022-10-21  15:15
 * @Description TODO
 * History:
 * <author> Administrator
 * <time> 2022-10-21  15:15
 * <version> 1.0.0
 * <desc> 版本描述
 */
package com.bosssoft.hr.train.j2se.example.pojo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 角色实体类
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Role extends BaseEntity {
    /**
     * 名字
     */
    private String name;
    /**
     * 代码
     */
    private String code;
    /**
     * 备注
     */
    private String remark;
}
