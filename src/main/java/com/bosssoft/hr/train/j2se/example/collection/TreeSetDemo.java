package com.bosssoft.hr.train.j2se.example.collection;

import com.bosssoft.hr.train.j2se.example.pojo.User;

import java.util.Comparator;
import java.util.TreeSet;

public class TreeSetDemo {
    // 按照工号排序
    TreeSet<User> userTreeSet = new TreeSet<>(Comparator.comparing(User::getCode));

    /**
     * 创建由User对象组成的树集
     *
     * @param number User对象的数量
     */
    public TreeSetDemo(int number) {
        // 使用TreeSet存储10个User
        for (int index = 0; index < number; index++) {
            User user = new User((long) index, Integer.toString(index), Integer.toString(index));
            userTreeSet.add(user);
        }
    }

    /**
     * 在命令行输出树集中的所有User实例的名字
     */
    public void print() {
        // 迭代输出
        for (User user : userTreeSet) {
            System.out.println(user.getName());
        }
    }
}
