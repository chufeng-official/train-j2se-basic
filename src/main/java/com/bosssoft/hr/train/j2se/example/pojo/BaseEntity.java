/**
 * Copyright (C), 2001-2022, www.bosssof.com.cn
 * FileName: BaseEntity
 * Author: Administrator
 * Date: 2022-10-21 14:38:16
 * Description:
 * <p>
 * History:
 * <author> Administrator
 * <time> 2022-10-21 14:38:16
 * <version> 1.0.0
 * <desc> 抽取的公共的字段所有的子entity从这里集成避免冗余
 */
package com.bosssoft.hr.train.j2se.example.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * @ClassName: BaseEntity
 * @Description: 定义所有的entity的父类，减少代码容易，该类主要针对表的公共字段
 * 该类为抽象类不允许被继承所以定义为abstract class
 * lombok注解@Data 包含了 @getter @setter  @ToString @HashAndEqual 等
 * @Author: Administrator
 * @Date: 2022-10-21 14:38
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseEntity implements Serializable {
    /**
     * 实体主键
     */
    protected Long id;
    /**
     * 租户id
     */
    private Long tenantId;
    /**
     * 机构id
     */
    private Long orgId;
    /**
     * 公司id
     */
    private Long companyId;
    /**
     * 创建者id
     */
    private Long createdBy;
    /**
     * 创建者名
     */
    private String creator;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新者id
     */
    private Long updatedBy;
    /**
     * 更新时间
     */
    private Date updatedTime;
    /**
     * 更新人
     */
    private String modifier;
    /**
     * 状态。1：正常用户；2：注销用户
     */
    private byte status;
    /**
     * 版本号
     */
    private Long version;

    @Override
    public int hashCode() {
        return Objects.hashCode(this.id);
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof BaseEntity)) {
            return false;
        }
        return Objects.equals(this, object);
    }
}
