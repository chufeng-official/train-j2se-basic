package com.bosssoft.hr.train.j2se.example.collection;

import com.bosssoft.hr.train.j2se.example.pojo.User;

import java.util.HashMap;

/**
 * 哈希表作业
 **/
public class HashMapDemo {
    HashMap<Long, User> userHashMap = new HashMap<>();

    /**
     * 创建键为用户ID、值为User对象的哈希表
     *
     * @param number User对象的数量
     */
    public HashMapDemo(int number) {
        // 使用HashMap存储10个User，Map的key为用户id，value为对象
        for (int index = 0; index < number; index++) {
            User user = new User((long) index, Integer.toString(index), Integer.toString(index));
            userHashMap.put(user.getId(), user);
        }
    }

    /**
     * 在命令行输出哈希表中的所有User实例的名字
     */
    public void print() {
        // 迭代输出
        for (User user : userHashMap.values()) {
            System.out.println(user.getName());
        }
    }
}
