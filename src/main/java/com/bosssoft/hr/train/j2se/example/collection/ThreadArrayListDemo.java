/**
 * Copyright (C), 2001-2022, www.bosssof.com.cn
 * FileName: ThreadArrayListDemo
 * Author: Administrator
 * Date: 2022-10-24 15:07:28
 * Description:
 * <p>该文件对应java基础作业 4)	ThreadArrayListDemo.java
 * History:
 * <author> Administrator
 * <time> 2022-10-24 15:07:28
 * <version> 1.0.0
 * <desc> 作业初始化的版本未来将会更迭
 */
package com.bosssoft.hr.train.j2se.example.collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * 多线程作业
 */
public class ThreadArrayListDemo {
    /**
     * 启动多线程作业任务
     *
     * @throws InterruptedException 线程中断错误
     */
    public void startTesting() throws InterruptedException {
        List<String> productList = Collections.synchronizedList(new ArrayList<>());
        CountDownLatch countDownLatch = new CountDownLatch(2);
        new ProducerThread(productList, countDownLatch).start();
        new ConsumerThread(productList, countDownLatch).start();
        countDownLatch.await();
    }

    /**
     * 生产者线程
     */
    public static class ProducerThread extends Thread {
        /**
         * 线程计数器
         */
        private final CountDownLatch countDownLatch;
        /**
         * 产品列表
         */
        private final List<String> productList;

        public ProducerThread(List<String> productList, CountDownLatch countDownLatch) {
            this.countDownLatch = countDownLatch;
            this.productList = productList;
        }

        /**
         * 重写run方法
         */
        @Override
        public void run() {
            super.run();
            int i = 1;
            try {
                while (true) {
                    Thread.sleep(1000);
                    productList.add("product" + i);
                    System.out.println("添加了" + "product" + i);
                    i++;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                countDownLatch.countDown();
            }
        }
    }

    /**
     * 消费者线程
     */
    public static class ConsumerThread extends Thread {
        /**
         * 线程计数器
         */
        private final CountDownLatch countDownLatch;
        /**
         * 产品列表
         */
        private final List<String> productList;

        public ConsumerThread(List<String> productList, CountDownLatch countDownLatch) {
            this.countDownLatch = countDownLatch;
            this.productList = productList;
        }

        /**
         * 重写run方法
         */
        @Override
        public void run() {
            try {
                while (true) {
                    Thread.sleep(1000);
                    if (productList.isEmpty()) {
                        continue;
                    }
                    System.out.println("移除了" + productList.get(0));
                    productList.remove(0);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                this.countDownLatch.countDown();
            }
        }
    }
}
